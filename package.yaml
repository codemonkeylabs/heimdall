name:        heimdall
version:     1.5.1.0
description: Highly Effective Intervention Manipulation and Data Analysis at the Link Layer
git:         https://gitlab.com/codemonkeylabs/heimdall
homepage:    https://gitlab.com/codemonkeylabs/heimdall#readme
bug-reports: https://github.com/codemonkeylabs/heimdall/issues
author:      Erick Gonzalez <erick@codemonkeylabs.de>
maintainer:  erick@codemonkeylabs.de
copyright:   2018-2019 Erick Gonzalez
license:     LGPL

extra-source-files:
    - README.md
    - ChangeLog.md
    - package.yaml

flags:
  debug:
    description: Enable debug messages (normally disabled due to performance considerations)
    default: false
    manual: true

dependencies:
    - async          >= 2.2.1 && < 2.3
    - attoparsec     >= 0.13 && < 2.0
    - base           >= 4.7 && < 5
    - binary         >= 0.8.6 && < 0.9
    - bytestring     >= 0.10.8 && < 0.11
    - clock          >= 0.7 && < 0.9
    - containers     >= 0.6 && < 0.7
    - failable       >= 1.2 && < 1.3
    - hashable       >= 1.2.7 && < 1.4
    - hedis          >= 0.10 && < 1.0
    - iproute        >= 1.7.7 && < 1.8
    - data-default   >= 0.7.1.1 && < 0.8
    - lens           >= 4.17 && < 6.0
    - mtl            >= 2.2.2 && < 2.3
    - optparse-applicative
    - n-core         >= 0.0.0.1 && < 1.0
    - network        >= 2.8 && < 3.2
    - structured-cli >= 2.8 && < 3.0
    - text           >= 1.2.4
    - transformers   >= 0.5.5 && < 0.6
    - the-count      >= 0.1 && <= 1.0
    - time           >= 1.8.0.2 && < 1.10
    - vector         >= 0.12 && < 0.13
    - unix           >= 2.7 && < 2.8

when:
    - condition: flag(debug)
      then:
        cc-options:  -D__DEBUG__
        cpp-options: -D__DEBUG__
        ghc-options:
            - -O0
      else:
        ghc-options:
            - -O2

ghc-options:
    - -Wall
    - -Werror
    - -fno-warn-orphans
    - -fobject-code
    - -optP-Wno-nonportable-include-path

default-extensions:
    - BangPatterns
    - FlexibleContexts
    - FlexibleInstances
    - ImplicitParams
    - LambdaCase
    - MultiParamTypeClasses
    - NamedFieldPuns
    - OverloadedStrings
    - RecordWildCards
    - ScopedTypeVariables
    - TemplateHaskell
    - TupleSections

library:
  source-dirs: lib
  dependencies:
    - conduit     >= 1.3.1 && < 1.4
    - connection  >= 0.2.8 && < 0.4
    - hslogger    >= 1.2 && < 1.4
    - process     >= 1.6.2 && < 1.7

executables:
  heimdall:
    main:                Main.hs
    source-dirs:         src/heimdall
    ghc-options:
        - -threaded
        - -rtsopts
        - -with-rtsopts=-N
    dependencies:
        - heimdall
        - hashtables     >= 1.2.3 && < 1.3
        - pretty-hex     >= 1.0 && < 1.2
        - ttl-hashtables >= 1.4 && < 1.5
        - tuple          >= 0.3 && < 0.4
    c-sources:
        - native/src/heimdall.c
        - native/src/huvud.c
        - native/src/l3.c
        - native/src/l4.c
    include-dirs:
        - native/include
    when:
        - condition: os(linux)
          then:
            dependencies:
              - network-packet-linux >= 0.1.1 && < 1.2
            cc-options:      -DLINUX
            cpp-options:     -DLINUX
            c-sources:
                - native/src/nfq.c
            include-dirs:
                - native/include
            extra-libraries:
                - netfilter_queue
                - nfnetlink
          else:
            c-sources:
                - native/src/pcap.c
            include-dirs:
                - native/include
            extra-libraries: pcap

  hofund:
    main:                Main.hs
    source-dirs:         src/hofund
    ghc-options:
        - -threaded
        - -rtsopts
        - -with-rtsopts=-N
    dependencies:
        - heimdall

tests:
  heimdall-test:
    main:                Spec.hs
    source-dirs:         test
    ghc-options:
    - -threaded
    - -rtsopts
    - -with-rtsopts=-N
    dependencies:
    - heimdall
