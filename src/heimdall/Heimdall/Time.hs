module Heimdall.Time (getHashedTime,
                      keeper) where

import Control.Concurrent.Async (Async, async)
import Control.Concurrent       (threadDelay)
import Control.Monad.IO.Class   (MonadIO, liftIO)
import Data.Function            (fix)
import Data.Hashable            (hash)
import Data.IORef               (IORef, newIORef, readIORef, atomicWriteIORef)
import Data.Time                (UTCTime(..), getCurrentTime, formatTime,
                                 defaultTimeLocale, nominalDiffTimeToSeconds)
import Data.Time.Clock.POSIX    (utcTimeToPOSIXSeconds)
import System.IO.Unsafe         (unsafePerformIO)
import Control.Arrow

import qualified Data.ByteString.Lazy.Char8 as L

type HashedTime = (Int, L.ByteString)

{-# NOINLINE utcTime #-}
utcTime :: IORef HashedTime
utcTime = unsafePerformIO $
  newIORef =<< getHashedTime'

getHashedTime :: MonadIO m => m HashedTime
getHashedTime = liftIO $ readIORef utcTime

getHashedTime' :: IO HashedTime
getHashedTime' = (arr hashWith &&& arr fmtTime) <$> getCurrentTime
  where hashWith                   = hash . hourlyStamp
        hourlyStamp (UTCTime d s)  =
                   nominalDiffTimeToSeconds
                 . utcTimeToPOSIXSeconds
                 . UTCTime d . fromIntegral $ (round s `div` 600 * 600 :: Int)

keeper :: IO (Async ())
keeper = async . fix . (>>) $ update
  where update = updateClockingTimestamp >> threadDelay 100000

updateClockingTimestamp :: IO ()
updateClockingTimestamp =
  atomicWriteIORef utcTime =<< getHashedTime'

fmtTime :: UTCTime -> L.ByteString
fmtTime  = L.pack . formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S"

