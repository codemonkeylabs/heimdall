{-# LANGUAGE CPP, OverloadedStrings #-}
{- |
Description: Heimdall Virtual Interfaces
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.VIFs where

import Control.Concurrent          (threadDelay)
import Control.Concurrent.Async    (Async, AsyncCancelled, async)
import Control.Concurrent.MVar     (MVar, newEmptyMVar, putMVar, withMVar)
import Control.Exception           (Handler(..),
                                    SomeException,
                                    bracket,
                                    catches,
                                    throw,
                                    uninterruptibleMask_)
import Control.Lens                ((^.), views)
import Control.Monad               (forever, void)
import Control.Monad.Failable      (Failable, failableIO, failure, hoist)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Trans         (lift)
import Control.Monad.Trans.Maybe   (MaybeT(..), runMaybeT)
import Data.ByteString.Char8       (ByteString, length, pack, unpack)
import Data.ByteString.Unsafe      (unsafePackCStringLen)
import Data.Configurable           (deserialize)
import Data.Foldable               (traverse_)
import Data.Maybe                  (fromMaybe)
import Foreign.Ptr                 (Ptr)
import Foreign.C.Types
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces
#ifdef LINUX
import Heimdall.Interface.VIF.Linux   (platformVIFStart, platformVIFStop)
#else
import Heimdall.Interface.VIF.Generic
#endif
import Heimdall.Interface.VIF.Types
import Heimdall.Interface.Utils
import Heimdall.Network.Foreign.Utils (bindToDevice, bindToDevice', setConnectionTag)
import Heimdall.Packet
import Heimdall.Routing
import Heimdall.Types
import Network.Address      as NA  hiding (Interface)
import Network.Socket       as NS  (SocketType(Raw))

import Network.Socket.ByteString   (sendAllTo, recv)
import Prelude hiding              (length)
import System.Logging       hiding (monitor)

foreign import ccall "vif_tx" hw_vif_tx :: Ptr CChar -> CInt -> CInt -> CInt -> IO Int

vifConfigPrefix :: ByteString
vifConfigPrefix = intfKeyPrefix <> "vif."

runVIFs :: (?env::Env) => IO (Async ())
runVIFs = async $
  runInterfaces "VIFs" [vifConfigPrefix] VIFIntf runVIF stopVIF

runVIF :: (?env::Env) => StartFn
runVIF name' info = do
  let node' = fromMaybe localNode $ lookup "node" info
  if localNode == node'
    then runVIF'
    else return Nothing
        where runVIF' = do
                    tID <- async $ serve name' info `catches` [
                           Handler $ \(e::AsyncCancelled) -> do
                             infoM "VIFs" $ "Stopped service for VIF " ++ vifId' ++ ": " ++ show e
                             throw e,
                           Handler $ \(e::SomeException) -> do
                             warningM "VIFs" $ "Failure servicing VIF " ++ vifId' ++ ": " ++ show e
                             threadDelay 3000000
                          ]
                    return $ Just tID
              vifId' = views intfId show name'
              localNode = pack $ getNode ?env

stopVIF :: StopFn
stopVIF _ = return ()

serve :: (?env::Env) => IntfName -> [(ByteString, ByteString)] -> IO ()
serve name' info = bracket create destroy (service name') >> threadDelay 2300000
    where create = do
            skVar <- newEmptyMVar
            idb   <- getIDB name'
            let mode = fromMaybe TxMode $ deserialize =<< lookup "mode" info
            intf  <- importIntf name' info (txPacket skVar) $ toCapabilities mode
            registerIntf intf
            let devName = lookup "device" info
                devType = lookup "type" info
                devPeer = lookup "peer" info
            devIPs     <- deserialize $ fromMaybe "" $ lookup "ip-addresses" info
            devPeerIPs <- deserialize $ fromMaybe "" $ lookup "peer-ip-addresses" info
            return VIF { vifId          = vifId',
                         vifDevName     = devName,
                         vifDevType     = devType,
                         vifDevPeer     = devPeer,
                         vifDevIPs      = devIPs,
                         vifDevPeerIPs  = devPeerIPs,
                         vifIDB         = idb,
                         vifMode        = mode,
                         vifSkVar       = skVar }
          destroy _ =
            infoM "VIFs" $ "Shutting down " ++ show name'
          vifId'   = name' ^. intfId
          toCapabilities TxMode = can TxData
          toCapabilities RxMode = can RxData
          toCapabilities DuplexMode = can RxData <> can TxData

service :: IntfName -> VIF -> IO ()
service vifName vif@VIF{..} = do
  infoM "VIFs" $ "Running service for " ++ name'
  let initTxRx = runExceptT $ do
                   infoM "VIFs" $ "Configuring platform for " ++ name'
                   failableIO $ platformVIFStart vif
                   txV4Socket <- whenTx . failableIO $ socket AF_INET NS.Raw ipRawProto
                   txV6Socket <- whenTx . failableIO $ socket AF_INET6 NS.Raw ipRawProto
                   rxV4Socket <- whenRx . failableIO $ socket AF_PACKET Datagram ipPacketProto
                   void . runMaybeT $ do
                     devName <- MaybeT $ return vifDevName
                     let devName' = unpack devName
                     infoM "VIFs" $ "Binding " ++ name' ++ " to " ++ devName'
                     lift $ do
                       traverse_ (`bindToDevice` devName') txV4Socket
                       traverse_ (`bindToDevice` devName') txV6Socket
                       traverse_ (`bindToDevice'` devName') rxV4Socket
                   void . runMaybeT $ do
                     Interface {..} <- getIntfM vifIDB
                     tag            <- MaybeT $ return _intfConnTag
                     infoM "VIFs" $ "Setting connection tag " ++ show tag ++ " for " ++ name'
                     lift $ do
                       traverse_ (`setConnectionTag` tag) txV4Socket
                       traverse_ (`setConnectionTag` tag) txV6Socket
                   return $ VIFSockets txV4Socket txV6Socket rxV4Socket
      stopTxRx (Left _) = return ()
      stopTxRx (Right (VIFSockets txV4Socket txV6Socket rxV4Socket)) = do
        infoM "VIFs" $ "Stopping service for " ++ name'
        result <- runExceptT $
          failableIO $ do
            platformVIFStop vif
            traverse_ close txV4Socket
            traverse_ close txV6Socket
            traverse_ close rxV4Socket
        either (failed "stopping VIF") return result
  bracket initTxRx (uninterruptibleMask_ . stopTxRx) $ either initFailure $ \sk -> do
    infoM "VIFs" $ "Starting " ++ name'
    putMVar vifSkVar sk
    infoM "VIFs" $ name' ++ " is ready"
    traverse_ (receive name' vifIDB) (vifSocketRxV4 sk)
    forever . threadDelay $ maxBound
      where initFailure err  =
              warningM "VIFs" $ "Unable to service VIF interface " ++ name' ++ ": " ++ show err
            ipRawProto       = fromIntegral . fromEnum $ NA.Raw
            ipPacketProto    = fromIntegral . fromEnum $ PacketProto 
            name'             = show vifName
            failed intro err = warningM "VIFs" $ "Failed " ++ intro ++ ": " ++ show err
            whenTx
              | vifMode `elem` [TxMode, DuplexMode] = fmap Just
              | otherwise = const $ return Nothing
            whenRx
              | vifMode `elem` [RxMode, DuplexMode] = fmap Just
              | otherwise = const $ return Nothing

receive :: String -> IDB -> Socket -> IO ()
receive name' idb sk = do
  payload <- recv sk maxPacketSize
  debugM "VIFs" $ "Received " ++ show (length payload) ++ " bytes on " ++ name'
  handleRxPacket name' idb (selectRP idb) payload `catches` [
    Handler $ \(e::AsyncCancelled) -> throw e,
    Handler $ \(e::SomeException) ->
        infoM "VIFs" $ "Failure routing packet: " ++ show (e::SomeException)
    ]
  receive name' idb sk
  where maxPacketSize = 2048

txPacket :: MVar VIFSockets -> Packet -> Interface -> IO ()
txPacket skVar pkt@Packet{..} intf =
  withMVar skVar $ \sks -> do
    sk <- getTxSocketFor getFamily sks
    txPacket' sk pkt intf

txPacket' :: Socket -> Packet -> Interface -> IO ()
txPacket' sk Packet{..} _intf
  | IPv4 addr <- getDstIP =
      void $ withFdSocket sk $ hw_vif_tx getPktStart (fromIntegral getPktLen) (fromIntegral $ toHostAddress addr)
  | IPv6 addr <- getDstIP = do
      bytes <- unsafePackCStringLen (getPktStart, fromIntegral getPktLen)
      let sockAddr = SockAddrInet6 defaultPort 0 (toHostAddress6 addr) 0
      void $ sendAllTo sk bytes sockAddr

getTxSocketFor :: Failable m => Family -> VIFSockets -> m Socket
getTxSocketFor AF_INET  = hoist uninitializedSocket . vifSocketTxV4
getTxSocketFor AF_INET6 = hoist uninitializedSocket . vifSocketTxV6
getTxSocketFor family'  = const . failure $ UnsupportedAddressFamily family'

uninitializedSocket :: e -> Exception
uninitializedSocket _ = InternalError "Uninitialized socket"
