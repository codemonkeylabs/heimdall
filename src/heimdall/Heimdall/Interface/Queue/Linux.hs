  module Heimdall.Interface.Queue.Linux where

import Control.Concurrent.MVar         (MVar, newMVar, modifyMVar, modifyMVar_)
import Control.Exception               (throw)
import Control.Monad                   (guard, void, when)
import Control.Monad.Trans             (lift)
import Control.Monad.Trans.Maybe       (MaybeT(..), runMaybeT)
import Control.Monad.Writer            (Writer, runWriter, tell)
import Data.List                       (uncons)
import Data.Print                      (toString)
import Data.Word                       (Word16)
import Foreign.C.String                (CString)
import Foreign.Ptr                     (Ptr, nullPtr)
import Heimdall.ACLs                   (getOrFetchACL)
import Heimdall.Common.ACL             (aceFamily)
import Heimdall.Exceptions
import Heimdall.Interface.Queue.Types
import Heimdall.Types
import Network.Address                 (Error(..), Family(..), Protocol(..))
import System.Exit                     (ExitCode(..))
import System.Logging
import System.Utils                    (execExclusively)
import System.IO.Unsafe                (unsafePerformIO)

foreign import ccall "heimdall.h queue_init"    queue_init    :: Word16 ->
                                                                 CString ->
                                                                 CString ->
                                                                 CString ->
                                                                 IO (Ptr HuvudQueue)
foreign import ccall "heimdall.h queue_destroy" queue_destroy :: Ptr HuvudQueue -> IO ()


data NFQState = NFQState {
                  nextId :: Int,
                  availableQs :: [[(Int, Ptr HuvudQueue)]]
}

{-# NOINLINE nfQStateVar #-}
nfQStateVar :: MVar NFQState
nfQStateVar = unsafePerformIO $
  newMVar NFQState {
    nextId      = 0,
    availableQs = mempty
  }

acquireHQ :: IO (Maybe [(Int, Ptr HuvudQueue)])
acquireHQ =
  modifyMVar nfQStateVar $ \nfqState@NFQState {..} -> return $
    flip (maybe (nfqState, Nothing)) (uncons availableQs) $ \(hq, rest) ->
      (nfqState { availableQs = rest }, return hq)

reserveHQ :: IO Int
reserveHQ =
  modifyMVar nfQStateVar $ \nfqState@NFQState {..} ->
    if nextId > fromIntegral (maxBound :: Word16)
      then throw NoQueueException
      else return (nfqState { nextId = nextId + 1}, nextId)

releaseHQ :: [(Int, Ptr HuvudQueue)] -> IO ()
releaseHQ hqs =
  modifyMVar_ nfQStateVar $ \nfqState@NFQState {..} ->
    return $ nfqState { availableQs = hqs : availableQs }

targetChain :: (?queue::Queue) => String
targetChain = qName ?queue

(<+>) :: Writer String () -> String -> Writer String ()
(<+>) actions str = actions >> tell (' ':str)

baseCmd :: (?table :: String) => String -> String -> Writer String ()
baseCmd oper chain =
  tell "-t" <+> ?table <+> oper <+> chain

qChainRule :: (?table :: String, ?chain::String, ?queue::Queue) => String -> Writer String ()
qChainRule oper =
  baseCmd oper ?chain  <+> "-j" <+> targetChain

qChain :: (?table :: String, ?chain::String) => String -> Writer String ()
qChain oper = baseCmd oper ?chain

insertQueueChainRule :: (?table::String, ?queue::Queue) => ServiceType -> ACE -> Writer String (Maybe Family)
insertQueueChainRule direction ace@ACE {..} = do
    baseCmd "-A" targetChain
    void . runMaybeT $ do
      proto <- MaybeT . pure $ _aceProtocol
      guard (proto == TCP || proto == UDP)
      let protoStr = show proto
      lift $ tell " -p" <+> protoStr <+> "-m" <+> protoStr
    perhaps "--dport" _acePorts
    perhaps "-s" _aceSrcRange
    perhaps "-d" _aceDstRange
    perhaps directionOp _aceDevice
    perhaps "-m mark --mark " _aceConnTag
    if _aceAccess == Permit
      then tell " -j NFQUEUE --queue-balance" <+> queues
      else tell " -j RETURN"
    return $ aceFamily ace
        where perhaps _ Nothing    = return ()
              perhaps arg (Just x) = tell (' ':arg) <+> toString x
              directionOp = case direction of
                              IngressService    -> "-i"
                              EgressService     -> "-o"
                              ForwardingService -> "-i"
              queues = show firstQ ++ ':':lastQ
              firstQ = show . fst . head $ qHQs ?queue
              lastQ  = show . fst . head . reverse $ qHQs ?queue

platformQueueStart :: (?queue::Queue) => IO ()
platformQueueStart = do
  let ?table = table
      ?chain = chain
  platformQueueStop
  createQueueChain
  insertQueueChainJump
  insertQueueChainRules
      where (table, chain) = getChainFor $ qServiceType ?queue

platformQueueStop :: (?queue::Queue) => IO ()
platformQueueStop = do
  let ?table = table
      ?chain = chain
  deleteQueueChainJump
  deleteQueueChain
      where (table, chain) = getChainFor $ qServiceType ?queue

getChainFor :: ServiceType -> (String, String)
getChainFor IngressService    = ("raw", "PREROUTING")    -- thinking, before it goes thru CONNTRACK
getChainFor EgressService     = ("mangle", "POSTROUTING")
getChainFor ForwardingService = ("mangle", "FORWARD")

deleteQueueChainJump :: (?table::String, ?chain::String, ?queue::Queue) => IO ()
deleteQueueChainJump = iptables $ qChainRule "-D" >> return Nothing

insertQueueChainJump :: (?table::String, ?chain::String, ?queue::Queue) => IO ()
insertQueueChainJump = iptables $ qChainRule "-A" >> return Nothing

deleteQueueChain :: (?table::String, ?queue::Queue) => IO ()
deleteQueueChain = do
  let ?chain = targetChain
  iptables $ qChain "-F" >> return Nothing
  iptables $ qChain "-X" >> return Nothing

createQueueChain :: (?table::String, ?queue::Queue) => IO ()
createQueueChain = do
  let ?chain = targetChain
  iptables $ qChain "-N" >> return Nothing

insertQueueChainRules :: (?table::String, ?queue::Queue) => IO ()
insertQueueChainRules =
  void. runMaybeT $ do
    aclRef  <- MaybeT . pure $ qACL ?queue
    ACL{..} <- MaybeT $ getOrFetchACL aclRef
    lift $ mapM_ (iptables . insertQueueChainRule serviceType) $ fmap snd aclRules
        where serviceType = qServiceType ?queue

iptables :: Writer String (Maybe Family) -> IO ()
iptables actions = do
  let (family, cmd) = runWriter actions
  mapM_ runOnTable $ (++ cmd) <$> onTablesFor family
    where runOnTable cmd = do
            debugM "IPTables.Queues" $ "Issuing " ++ cmd
            result <- execExclusively 2000000 cmd
            when (result /= Just ExitSuccess) $
              infoM "IPTables.Queues" $ "Command " ++ cmd ++ " failed: " ++ show result
          onTablesFor (Just AF_INET)  = ["sudo iptables -w "]
          onTablesFor (Just AF_INET6) = ["sudo ip6tables -w "]
          onTablesFor (Just family)   = throw $ UnsupportedAddressFamily family
          onTablesFor Nothing         = onTablesFor (Just AF_INET) ++ onTablesFor (Just AF_INET6)

platformQueueInit :: Int -> Int -> CString -> CString -> CString -> IO [(Int, Ptr HuvudQueue)]
platformQueueInit _ numWorkers component device filterExp = do
  mHQ <- acquireHQ
  maybe createHQ return mHQ
    where createHQ = do
            iD <- (numWorkers *) <$> reserveHQ
            mapM initQ  [iD .. iD + numWorkers - 1]
          initQ iD = do
            warningM "Queues" $ "Initializing HW Q " ++ show iD
            hq <- queue_init (fromIntegral iD) component device filterExp
            when (hq == nullPtr) $ do
              errorM "Queues" $ "Failed to create queue #" ++ (show iD) ++
                 ". Most likely cause is missing CAP_NET_ADMIN capability"
              throw NoQueueException
            return (iD, hq)

platformQueueRelease :: [(Int, Ptr HuvudQueue)] -> IO ()
platformQueueRelease = releaseHQ
