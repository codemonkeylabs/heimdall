module Heimdall.Interface.Queue.Types where

import Foreign.Ptr                   (Ptr)
import Heimdall.Types

data HuvudQueue

data Queue = Queue  { qId          :: Int,
                      qDev         :: String,
                      qName        :: String,
                      qBpf         :: String,
                      qIDB         :: IDB,
                      qServiceType :: ServiceType,
                      qACL         :: Maybe ACLRef,
                      qNumWorkers  :: Int,
                      qHQs         :: [(Int, Ptr HuvudQueue)] }

