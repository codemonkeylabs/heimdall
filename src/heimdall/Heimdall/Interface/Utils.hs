{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall interface manipulation and helper functions
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.Utils where

import Control.Exception              (throw)
import Control.Lens                   (preview)
import Control.Monad                  ((<=<), join)
import Control.Monad.Failable         (Failable, hoist)
import Control.Monad.IO.Class         (MonadIO)
import Control.Monad.Trans.Maybe      (MaybeT(..), runMaybeT)
import Data.ByteString.Char8          (ByteString, unpack)
import Data.Configurable              (deserialize)
import Data.Count                     (newCount)
import Data.IP                        (IP)
import Data.IORef                     (newIORef)
import Data.Maybe                     (fromMaybe)
import Heimdall.ACLs                  (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces            (getIDB, getIntf)
import Heimdall.PolicyMaps
import Heimdall.Types
import System.Logging

import qualified Data.Attoparsec.ByteString.Char8 as A

importIntf :: (?env::Env) => IntfName
                          -> [(ByteString, ByteString)]
                          -> Forwarder
                          -> Capabilities
                          -> IO Interface
importIntf _intfName info _intfForwarder _intfCapabilities = do
  let _intfVRFId        = fromMaybe 0 $ lookup "vrf-id" info >>= deserialize
      _intfConnTag      = lookup "connection-tag" info >>= deserialize
      _intfMasquerading = fromMaybe False $ lookup "masquerading" info >>= deserialize
      _intfTrackStats   = fromMaybe False $ lookup "track-stats" info >>= deserialize
      rPath             = deserialize =<< lookup "rpath" info
      deflector         = deserialize =<< lookup "deflector" info
      logACL            = hoist id $ lookup "log-acl" info
  _intfPolicyMap <- importPolicyMap _intfName
  _intfLogACL    <- runMaybeT $ getACLRef =<< logACL
  _intfRP        <- mapM getIDB rPath
  _intfDeflector <- mapM getIDB deflector
  _intfCounter   <- newIORef Nothing
  infoM "Interfaces" $ "Importing interface " ++ show _intfName
  return Interface {..}

getIPAddr :: (Failable m) => [(ByteString, ByteString)] -> ByteString -> m IP
getIPAddr info property = do
  str <- hoist missing $ lookup property info
  deserialize str
      where missing () = InvalidOrMissingConfig $ unpack property

getIntfName :: ByteString -> ByteString -> IntfName
getIntfName typeStr idStr =
  fromMaybe badId . hoist InvalidValue $ A.parseOnly parseIntfName (typeStr <> "/" <> idStr)
     where badId = throw . InvalidOrMissingConfig $ "Malformed " ++ unpack typeStr
                   ++ "interface ID " ++ unpack idStr

selectRP :: (MonadIO m, Failable m) => IDB -> m (Maybe IDB)
selectRP =  rpFrom <=< getIntf
  where rpFrom = return . join . preview (traverse . intfRP)
