module Heimdall.Interface.VIF.Types where

import Control.Concurrent.MVar  (MVar)
import Control.Monad.Failable   (failure)
import Data.ByteString          (ByteString)
import Data.ByteString.Char8    (unpack)
import Data.Configurable        (Configurable(..))
import Data.IP                  (IP)
import Heimdall.Exceptions      (Exception(..))
import Heimdall.Types           (IDB)
import Network.Socket           (Socket)

data VIF = VIF { vifId          :: Int,
                 vifDevName     :: Maybe ByteString,
                 vifDevType     :: Maybe ByteString,
                 vifDevPeer     :: Maybe ByteString,
                 vifDevIPs      :: [IP],
                 vifDevPeerIPs  :: [IP],
                 vifIDB         :: IDB,
                 vifMode        :: VIFMode,
                 vifSkVar       :: MVar VIFSockets }

data VIFSockets = VIFSockets { vifSocketTxV4 :: Maybe Socket,
                               vifSocketTxV6 :: Maybe Socket,
                               vifSocketRxV4 :: Maybe Socket }

data VIFMode = RxMode | TxMode | DuplexMode
  deriving (Eq, Show)

instance Configurable VIFMode where
    serialize RxMode     = return "rx"
    serialize TxMode     = return "tx"
    serialize DuplexMode = return "duplex"
    deserialize "rx"     = return RxMode
    deserialize "tx"     = return TxMode
    deserialize "duplex" = return DuplexMode
    deserialize str      = failure . InvalidValue $ "not a mode: " <> unpack str
