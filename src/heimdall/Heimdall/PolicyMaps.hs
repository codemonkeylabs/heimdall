module Heimdall.PolicyMaps where

import Prelude              hiding (dropWhile, reverse)
import Control.Monad.IO.Class      (MonadIO)
import Control.Monad.Failable      (Failable(..))
import Data.ByteString.Char8       (ByteString, dropWhile, reverse)
import Data.Configurable           (deserialize)
import Data.Char                   (isSpace)
import Data.List                   (sortOn)
import Data.IORef                  (newIORef)
import Data.Referable              (Dict, Referable(..))
import Database.Adapter            (getMap)
import Heimdall.Common.ACL         ()
import Heimdall.Environment
import Heimdall.Types
import System.IO.Unsafe            (unsafePerformIO)

import qualified Data.Map       as Map
type PolicyRefs = Dict ByteString Policy

{-# NOINLINE policies #-}
policies :: PolicyRefs
policies = unsafePerformIO $ newIORef Map.empty

getPolicyRef :: (MonadIO m) => ByteString -> m PolicyRef
getPolicyRef = getRef policies

getPolicyMapEntry :: (MonadIO m, Failable m) => (ByteString, ByteString) -> m PolicyMapEntry
getPolicyMapEntry (aceStr, policyName) = do
  aces      <- deserialize aceStr
  policyRef <- getPolicyRef policyName
  return (aces, policyRef)

getPolicyMapRule :: (MonadIO m, Failable m) => (ByteString, ByteString) -> m PolicyMapRule
getPolicyMapRule (priorityStr, entryStr) = do
  priority             <- deserialize priorityStr
  (aceStr, policyName) <- deserialize entryStr
  entry <- getPolicyMapEntry (trim aceStr, trim policyName)
  return (priority, entry)
    where trim = snip . snip
          snip = reverse . dropWhile isSpace

importPolicyMap :: (?env::Env, MonadIO m, Failable m) => IntfName -> m PolicyMap
importPolicyMap name = do
  let policyMapName = mkKeyFromIntfName name <> ".policies"
  policyRules <- getMap db policyMapName
  sortOn fst <$> mapM getPolicyMapRule policyRules
    where db = getDB ?env
