{-# LANGUAGE CPP #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{- |
Description: Heimdall Routing
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Heimdall.Routing (Result(..), handleRxPacket, routePacket) where

import Control.Applicative          ((<|>))
import Control.Exception            (Exception, SomeException, toException)
import Control.Lens
import Control.Monad                (MonadPlus, guard, foldM, forM_, void, when)
import Control.Monad.Except         (runExceptT)
import Control.Monad.Failable       (Failable(..), autohoist)
import Control.Monad.IO.Class       (MonadIO, liftIO)
import Control.Monad.State.Strict   (StateT, evalStateT, get)
import Control.Monad.Trans          (lift)
import Control.Monad.Trans.Maybe    (MaybeT(..), runMaybeT)
import Data.Typeable                (Typeable)
import Data.Bool                    (bool)
import Data.ByteString.Char8        (ByteString, unpack)
import Data.Count                   (count, (.=.))
import Data.Default                 (def)
import Data.Fn                      ((.-.))
import Data.Hashable                (hash)
import Data.IORef                   (atomicModifyIORef')
import Data.List                    (find)
import Data.Maybe
import Data.Print                   (toString)
import Data.Referable               (getRefName)
import Data.Vector                  ((!))
import Heimdall.ACLs                (getOrFetchACL)
import Heimdall.Connections
import Heimdall.Connection.Tracking (lookupConnection, updateConnection)
import Heimdall.Exceptions   hiding (Exception)
import Heimdall.Foreign.Packet      (withUnsafeL3Packet)
import Heimdall.Injector            (inject)
import Heimdall.Interfaces
import Heimdall.Packet              (Packet(..))
import Heimdall.Policies            (getOrFetchPolicy)
import Heimdall.RouteMaps           (fetchRouteMap, getOrFetchRouteMap)
import Heimdall.Stats
import Heimdall.Time
import Heimdall.Types        hiding (protocol)
import Network.Address       hiding (Interface)
import Network.Flow
import System.Logging

import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.IP.RouteTable         as RT
import qualified Data.IntMap                as IMap
import qualified Data.Vector                as V

data State = State { _interimResult :: Result,
                     _inputIntf     :: Interface,
                     _outputIntf    :: Maybe Interface,
                     _packet        :: Packet,
                     _doLog         :: Bool,
                     _deflected     :: Bool,
                     _doCount       :: Bool,
                     _connection    :: Connection,
                     _packetFlow    :: Flow,
                     _flowDirection :: Direction,
                     _matchedPolicy :: Maybe PolicyName,
                     _comment       :: Maybe ByteString }

makeLenses ''State

type RoutingT = StateT State

data RoutingException = NoMatch
                      | ResultNotOverridable
                      deriving (Typeable, Show, Eq)

instance Exception RoutingException

setResult :: (MonadIO m) => Result -> RoutingT m ()
setResult result = do
  previous <- use interimResult
  when (isOverridable previous) $ do
    mLog "Processing" $ "Routing result is now " ++ show result
    interimResult .= result

orientation :: (Field1 a a b b, Field2 a a b b) => Direction -> Lens' a b
orientation dir
 | dir == Upwards = upwards
 | otherwise      = downwards

routePacket :: (MonadIO m, Failable m, MonadPlus m) => IDB -> IO (Maybe IDB) -> Packet -> m Result
routePacket idb rPath pkt = do
  mIntf <- getIntf idb
  intf  <- maybe (failed "missing or disabled ingress interface") return mIntf
  (nowHash, now) <- getHashedTime
  let flow   = getFlow pkt
      vrfID  = intf ^. intfVRFId
      mVRFID = guard (vrfID /= 0) >> return vrfID
  fastTracked <- runMaybeT $ do
                   conn <- autohoist =<< lookupConnection mVRFID flow
                   dir  <- getDirection pkt conn
                   let actions = conn ^. verdict . orientation dir
                       logs    = conn ^. logging . orientation dir
                       connVRF = fromMaybe 0 $ getVRFID conn
                   if null actions
                     then routeSlowly pkt now connVRF intf flow conn
                     else do
                       when logs . infoM "Routing.Incoming" $
                         show (intf ^. intfName) ++ " => *fast* " ++ show pkt ++ " on " ++ toString conn
                       forM_ actions $ applyFast logs pkt intf
                       trackStats intf (conn ^. flowId) conn dir pkt now
                       return $ conn ^. final . orientation dir
  case fastTracked of
    Just result ->
      return result
    Nothing -> do
      let srcIP = getSrcIP pkt
      rp        <- liftIO rPath
      conn      <- autohoist =<< updateConnection vrfID flow (withConnection rp (vrfID, srcIP))
      routeSlowly pkt now vrfID intf flow conn
        where withConnection rp srcIP mConn =
                return . setSourceAddress srcIP $ fromMaybe (newConnection rp) mConn
              setSourceAddress ip conn@Connection{..}
                | isNothing _source = conn & source ?~ ip
                | otherwise         = conn
              newConnection rp =
                Connection {
                  _source        = Nothing   ,
                  _flowId        = 0         ,
                  _verdict       = bi mempty ,
                  _rpIDB         = rp        ,
                  _startTime     = now       ,
                  _startHash     = nowHash   ,
                  _final         = bi Drop   ,
                  _statistics    = bi mempty ,
                  _logging       = bi False
              }

routeSlowly :: (Failable m, MonadIO m, MonadPlus m) => Packet
                                                    -> L.ByteString
                                                    -> VRFID
                                                    -> Interface
                                                    -> Flow
                                                    -> Connection
                                                    -> m Result
routeSlowly pkt now vrfID intf flow conn = do
  dir       <- getDirection pkt conn
  wantsLogs <- shouldLog intf dir pkt
  let state0 = State { _interimResult = def,
                       _inputIntf     = intf,
                       _outputIntf    = Nothing,
                       _packet        = pkt,
                       _doLog         = wantsLogs,
                       _doCount       = intf ^. intfTrackStats, -- could be overriden by policy
                       _deflected     = False,
                       _connection    = conn,
                       _packetFlow    = flow,
                       _flowDirection = dir,
                       _matchedPolicy = Nothing,
                       _comment       = Nothing }
  withState state0 $ routePacket' vrfID intf now
    where withState  = flip evalStateT

shouldLog :: (Failable m, MonadIO m) => Interface -> Direction -> Packet -> m Bool
shouldLog intf dir pkt = fromMaybe False <$> runMaybeT shouldLog'
  where shouldLog' = do
          aclRef  <- autohoist $ intf ^. intfLogACL
          ACL{..} <- MaybeT $ getOrFetchACL aclRef
          void . autohoist $ matchACL intf dir pkt aclRules
          return True

routePacket' :: (MonadIO m, Failable m, MonadPlus m) => VRFID
                                                     -> Interface
                                                     -> L.ByteString
                                                     -> RoutingT m Result
routePacket' vrfID intf now
  | intf <?> can RxData = do
      conn <- use connection
      flow <- use packetFlow
      pkt  <- use packet

      mLog "Incoming" $ "Analyzing " ++ show (intf ^. intfName) ++ " => " ++ show pkt ++ " on " ++ toString conn

      applyPolicies (intf ^. intfPolicyMap) `recover` \err ->
        mLog "Outgoing" $ "Failed policy application: " ++ show err

      result      <- use interimResult
      shouldCount <- use doCount
      wantsLog    <- use doLog
      dir         <- use flowDirection
      oIntf       <- use outputIntf
      comments    <- use comment
      punted      <- use deflected

      mLog "Outgoing" $ "Routing result: " ++ show result ++ " for " ++ show pkt

      policyName <- use matchedPolicy

      let (hash', leftAddr, leftPort, rightAddr, rightPort)
            | dir == Upwards && flow ^. left . address == getSrcIP pkt ||
              dir == Downwards &&  flow ^. right . address == getSrcIP pkt =
                (halfHashWith timeHash left flow,
                 flow ^. left . address, flow ^. left . port,
                 flow ^. right . address , flow ^. right . port)
            | otherwise =
                (halfHashWith timeHash right flow,
                 flow ^. right . address , flow ^. right . port,
                 flow ^. left . address, flow ^. left . port)
          (counters, tags)
            | dir == Upwards =
                ([(RxPackets, 1)],
                 [(InterfaceField .=. intf ^. intfName),
                  (VRFField       .=. vrfID)])
            | otherwise =
                ([(TxPackets, 1)],
                    catMaybes
                    [(InterfaceField,) . L.pack . show <$> oIntf ^? _Just . intfName,
                     (VRFField,      ) . L.pack . show <$> oIntf ^? _Just . intfVRFId])

          counters'  = if punted then (DeflectedPackets, 1):counters else counters
          started    = conn ^. startTime
          timeHash   = conn ^. startHash
          tags'      = ((ActionField    , explainAction punted result):
                        (FlowField      .=. hash'             ):
                        (LeftIP         .=. leftAddr          ):
                        (LeftPortField  .=. leftPort          ):
                        (ProtocolField  .=. flow ^. protocol  ):
                        (RightIP        .=. rightAddr         ):
                        (RightPortField .=. rightPort         ):
                        (TypeField      , "l4"                ):
                        (FlowStart      ,         started     ):
                        tags ++ catMaybes [
                           (PolicyField , ) . L.fromStrict <$> policyName ,
                           (CommentField, ) . L.fromStrict <$> comments

                         ])
          stats | shouldCount = return (counters', tags')
                | otherwise   = Nothing
      conn' <- updateConnection vrfID flow $
                 over _Just $ (logging    . orientation dir .~ wantsLog)
                            . (final      . orientation dir .~ result)
                            . (statistics . orientation dir .~ stats)
                            . (flowId                       .~ hash')

      when (isNothing conn') $ failure missingConnection

      when shouldCount $ trackStats intf hash' (fromJust conn') dir pkt now
      return result
  | otherwise = do
      pkt <- use packet
      mLog "Incoming" $ "Discarding unexpected packet on transmit only interface: " ++ show pkt
      return Drop
  where explainAction True _       = "Analyzed"
        explainAction False result = L.pack . show $ result

trackStats :: MonadIO m => Interface -> Int -> Connection -> Direction -> Packet -> L.ByteString -> m ()
trackStats intf xi conn dir pkt now
  | Just (counters, tags) <- conn ^. statistics . orientation dir = liftIO $ do
      let pktBytes  = fromIntegral $ getPktLen pkt
          counters' = ((RxBytes, pktBytes), (TxBytes, pktBytes)) ^. orientation dir :counters
          tags'     = (TimeField, now):tags
      atomicModifyIORef' (intf ^. intfCounter) (Nothing, ) >>= \case
        Nothing -> return ()
        Just theCount -> do
          count theCount xi counters' tags'
          atomicModifyIORef' (intf ^. intfCounter) $ const (return theCount, ())
  | otherwise = return ()

failed :: (MonadIO m, Failable m) => String -> m a
failed msg = do
  infoM "Routing" $ "Routing failure due to " ++ msg
  failure $ Aborted msg

missingConnection :: SomeException
missingConnection = toException $ InternalError "Missing connection"

mLog :: (MonadIO m) => String -> String -> RoutingT m ()
mLog section str = do
  wantsLog <- use doLog
  when wantsLog $ infoM ("Routing." ++ section) str

applyPolicies :: (MonadIO m, Failable m, MonadPlus m) => PolicyMap -> RoutingT m ()
applyPolicies policyMap = do
  pkt    <- use packet
  inIntf <- use inputIntf
  conn   <- use connection
  dir    <- use flowDirection
  flow   <- use packetFlow
  mLog "Processing" $ "Analyzing policies for " ++ toString dir ++ " " ++ toString flow
                   ++ " connection: " ++ toString conn ++ " on " ++ show (inIntf ^. intfName)
  let matchingACL = matchACL inIntf dir pkt
  void $ foldM (considerPolicy dir matchingACL) Nothing policyMap
    where considerPolicy dir matchingACL Nothing (ruleNum, (aces, policyRef)) = do
            mLog "Processing" $ "Considering policy in rule " ++ show ruleNum
                                    ++ " matching " ++ show aces
            result <- use interimResult
            if isOverridable result
              then do
                case matchingACL $ (ruleNum, ) <$> aces of
                  Just ace -> do
                    when (dir == Upwards) $ comment .= _aceComment ace
                    applyPolicy pName matchingACL =<< getOrFetchPolicy policyRef
                  _ ->
                    logFailure ruleNum pName
                return Nothing
              else do
                mLog "Processing" $ "Interim result " ++ show result ++ " is not overridable. Short circuiting policy evaluation"
                return $ Just ()
            where pName = unpack $ getRefName policyRef
          considerPolicy _ _ shortCircuit _ = return shortCircuit
          logFailure ruleNum pName =
             mLog "Processing" $ "Rejecting ACE " ++ show ruleNum ++ " skipping policy " ++ pName

applyPolicy :: (Failable m, MonadIO m) => String
                                       -> ([ACLRule] -> Maybe ACE)
                                       -> Maybe Policy
                                       -> RoutingT m ()
applyPolicy pName matchingACL (Just Policy{..}) = do
  matchedPolicy ?= policyName
  void $ foldM applyRule False policyRules
  where applyRule True _ =
            return True -- shortcircuit
        applyRule False (priority, (aces, actions)) =
          case matchingACL $ (priority,) <$> aces of
            Just _ -> do
              mLog "Processing" $ "ACE " ++ show priority ++ " in " ++ pName ++ " matches"
              mapM_ (applyAction False) actions
              return True -- shortcirtuit evaluation
            _ -> do
              mLog "Processing" $ "Skipping policy " ++ pName ++ " rule " ++ show priority
              return False
applyPolicy pName _ Nothing = do
  mLog "Processing" $ "Policy " ++ pName ++ " is not configured"

isOverridable :: Result -> Bool
isOverridable Bypass = True
isOverridable None   = True
isOverridable _      = False

matchACL :: Interface -> Direction -> Packet -> [ACLRule] -> Maybe ACE
matchACL intf dir = (shallPermit =<<) .-. matchingACE'
  where matchingACE'                 = find . matchingACE intf dir
        shallPermit (_, ace@ACE{..}) = if _aceAccess == Permit
                                         then return ace
                                         else Nothing

matchingACE :: Interface -> Direction -> Packet -> ACLRule -> Bool
matchingACE Interface{..} dir Packet{..} (_, ACE{..}) =
  let result = (_aceDirection >>= nothingIf (==) dir) <|>
               (_aceSrcRange  >>= nothingIfInRange getSrcIP) <|>
               (_aceDstRange  >>= nothingIfInRange getDstIP) <|>
               (_aceProtocol  >>= nothingIf protocolMatches getProto) <|>
               (_acePorts     >>= nothingIf portInRange (fromIntegral getDstPort)) <|>
               (_aceDevice    >>= nothingIf (==) (show _intfName))
      protocolMatches _ Internet = True
      protocolMatches a b = a == b
  in result /= Just False
     where nothingIfInRange (IPv4 ip) (IPv4Range range) = nothingIf isMatchedTo ip range
           nothingIfInRange (IPv6 ip) (IPv6Range range) = nothingIf isMatchedTo ip range
           nothingIfInRange _         _                 = return False
           nothingIf = ((bool (return False) Nothing .) .)

applyFast :: (MonadIO m, Failable m) => Bool -> Packet -> Interface -> ActionPlus -> m ()
applyFast wantsLog pkt rxIntf (ActionPlus _ (RouteVia idb)) = do
  result <- runMaybeT $ do
             intf <- getIntfM idb
             lift $ routeVia intf
  when (isNothing result && wantsLog) $
    infoM "Routing.Outgoing" $ "Dropping policy routed traffic out inactive or missing interface: " ++ show idb
      where routeVia oIntf@Interface{..} = do
              when wantsLog $ infoM "Routing.Outgoing" $ "*fast* routing packet " ++ show (getPktId pkt) ++ " -> " ++ show _intfName
              liftIO $ sendPacket pkt rxIntf oIntf
applyFast wantsLog pkt _ _ = do
  when wantsLog $ infoM "Routing.Processing" $ "Downgrading processing of " ++ show pkt ++ " to slow path"
  failure NotFast

applyAction :: (MonadIO m, Failable m) => Bool -> ActionPlus -> RoutingT m ()
applyAction True (ActionPlus wantsCount (RouteVia idb)) = do
  result <- runMaybeT $ do
             intf <- getIntfM idb
             lift $ routeVia intf
  when (isNothing result) $ do
    mLog "Outgoing" $ "Dropping policy routed traffic out inactive or missing interface: " ++ show idb
    setResult Drop
      where routeVia oIntf@Interface{..}
                | oIntf <?> can TxData = do
                    State {..} <- get
                    mLog "Outgoing" $ "Routing packet " ++ show (getPktId _packet) ++ " -> " ++ show _intfName
                    liftIO $ sendPacket _packet _inputIntf oIntf
                    outputIntf ?= oIntf
                    doCount %= (&& wantsCount)
                    setResult Routed
                | otherwise = do
                     mLog "Outgoing" $
                       "Discarding unroutable packet to receive only interface " ++ show _intfName
                     setResult Drop
applyAction cached (ActionPlus wantsCount (Loadbalance routeMapRef)) = do
  pkt   <- use packet
  flow  <- use packetFlow
  doCount %= (&& wantsCount)
  let i     = hash flow
      dstIP = getDstIP pkt
  conn  <- use connection
  result <- runMaybeT $ do
    vrfID <- getVRFID conn
    routingMap <- MaybeT (getOrFetchRouteMap routeMapRef) `recover` \_ -> do
                    lift missingMap
                    failure $ InvalidOrMissingConfig (unpack $ getRefName routeMapRef)
    routeVia <- MaybeT . pure $ lookupRoute vrfID dstIP routingMap
    let numRoutes         = V.length routeVia
        selectedRoute     = i `mod` numRoutes
        (oIntf, comments) = routeVia ! selectedRoute
    dir   <- lift $ use flowDirection
    when (dir == Downwards) $ comment ?= comments
    return $ ActionPlus wantsCount (RouteVia oIntf)
  maybe (noRoute dstIP) (applyAction cached) result
      where missingMap = do
              mLog "Outgoing" $ "Missing route map " ++ unpack (getRefName routeMapRef)
              setResult Drop
            noRoute dstIP = do
              mLog "Outgoing" $ "No route to " ++ show dstIP ++ " on route map "
                ++ unpack (getRefName routeMapRef) ++ ". Attempting route map re-fetch"
              void $ fetchRouteMap routeMapRef -- this should re fetch the routemap and
                                               -- even though it is too late for this pkt
                                               -- next one might hit a valid route if the
                                               -- route map was out of date
              setResult Drop
            lookupRouteMapTable vrfID RouteMap{..} =
              IMap.lookup (fromIntegral vrfID) routeMapTables
            lookupRoute vrfID (IPv4 ip) rMap = do
              v4Table <- fst <$> lookupRouteMapTable vrfID rMap
              RT.lookup (makeAddrRange ip 32) v4Table
            lookupRoute vrfID (IPv6 ip) rMap = do
              v6Table <- snd <$> lookupRouteMapTable vrfID rMap
              RT.lookup (makeAddrRange ip 128) v6Table
applyAction cached (ActionPlus wantsCount Forward) = do
  conn  <- use connection
  dir   <- use flowDirection
  doCount %= (&& wantsCount)
  if | dir == Upwards -> setResult Bypass
     | (Just rPath) <- conn ^. rpIDB -> applyAction cached $ ActionPlus wantsCount (RouteVia rPath)
     | otherwise -> do
         pkt   <- use packet
         mLog "Processing" $ "Missing rpath, going to drop packet" ++ show (getPktId pkt) ++ " and stop tracking: " ++ toString conn
         flow  <- use packetFlow
         vrfID <- getVRFID conn
         void $ updateConnection vrfID flow (const Nothing)
         setResult Drop
applyAction cached (ActionPlus wantsCount Deflect) = do
  doCount   %= (&& wantsCount)
  deflected .= True
  mDeflector <- use $ inputIntf . intfDeflector
  maybe noDeflector deflect mDeflector
    where noDeflector = do
            pkt <- use packet
            mLog "Processing" $ "Missing deflector IDB, dropping packet" ++ show (getPktId pkt)
            setResult Drop
          deflect = applyAction cached . ActionPlus wantsCount . RouteVia

applyAction True (ActionPlus wantsCount Continue) = setResult Bypass >> doCount %= (&& wantsCount)
applyAction True (ActionPlus wantsCount Discard)  = setResult Drop   >> doCount %= (&& wantsCount)
applyAction False action = do
  conn  <- use connection
  dir   <- use flowDirection
  flow  <- use packetFlow
  vrfID <- getVRFID conn
  void $ updateConnection vrfID flow $ over (_Just . verdict . orientation dir) (action:)
  applyAction True action

handleRxPacket :: (MonadIO m, Failable m) => String -> IDB -> IO (Maybe IDB) -> ByteString -> m ()
handleRxPacket name' idb rPath bytes = do
  result <- withUnsafeL3Packet bytes $ \pkt ->
    runExceptT $ do
      result' <- lift $ routePacket idb rPath pkt
      case result' of
        Abort ->
          failure . Aborted $ "Routing over interface " ++ name' ++ " has been aborted"
        Bypass ->
          inject pkt 0
        _ ->
          return ()
  either failure return result
