{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Routing Policies
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Policies where

import Prelude hiding (drop, dropWhile, lookup, reverse, span)
import Control.Concurrent.Async    (Async, async)
import Control.Exception           (throw)
import Control.Monad               (void)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO)
import Data.ByteString.Char8       (ByteString, stripPrefix, unpack)
import Data.Configurable           (deserialize)
import Data.List                   (sortOn)
import Data.Maybe                  (fromMaybe)
import Data.Referable              (Referable(..), delRef)
import Database.Adapter            (Adapter, getMap, new)
import Database.Adapter.Redis      (Redis)
import Heimdall.Common.ACL         ()
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces         (getIDB)
import Heimdall.ObjectModel        (monitor)
import Heimdall.PolicyMaps
import Heimdall.RouteMaps          (getRouteMapRef)
import Heimdall.Types
import System.Logging       hiding (monitor)

getOrFetchPolicy :: (MonadIO m, Failable m) => PolicyRef -> m (Maybe Policy)
getOrFetchPolicy ref = deRef ref >>= getOrFetch
  where getOrFetch Nothing = do
          (db :: Redis) <- readRedisConfig >>= new -- this is horrible but there is no faster cleaner solution
          deRef =<< importPolicy db (policyKeyPrefix <> getRefName ref)
        getOrFetch policy = return policy

updatePolicy :: (MonadIO m, Failable m) => ByteString -> Policy -> m PolicyRef
updatePolicy = updateRef policies

deletePolicy :: (MonadIO m) => ByteString -> m ()
deletePolicy = delRef policies

badAccess :: (MonadIO m) => String -> m ()
badAccess = throw . BadAccess

readPolicy :: (Adapter a, MonadIO m, Failable m) => a -> ByteString -> m Policy
readPolicy db name = do
  kvs     <- getMap db (policyKeyPrefix <> name)
  actions <- mapM (importRule name) kvs
  return Policy { policyName  = name,
                  policyRules = sortOn fst [ a | Just a <- actions ] }

importRule :: (MonadIO m) => ByteString -> (ByteString, ByteString) -> m (Maybe Rule)
importRule name (priorityStr, actionRuleStr) = do
  rule <- runExceptT $ do
           priority              <- deserialize priorityStr
           (aces, actionsList)   <- deserialize actionRuleStr
           actions               <- mapM instantiateAction =<< deserialize actionsList
           return (priority, (aces, actions))
  either badRule (return . Just) rule
      where badRule err = do
                warningM "Policies" $ "Invalid rule " ++ unpack priorityStr
                             ++ " in policy " ++ unpack name ++ ": " ++ unpack actionRuleStr
                             ++ ": " ++ show err
                return Nothing

badACE :: ByteString -> [(String, String)] -> Exception
badACE = (. show) . InvalidACE . unpack

instantiateAction :: (MonadIO m, Failable m) => ActionPlus -> m ActionPlus
instantiateAction (ActionPlus wantsCount (RouteVia deadIDBRef)) = do
  idb <- getIDB name
  return $ ActionPlus wantsCount (RouteVia idb)
    where name = getRefName deadIDBRef
instantiateAction (ActionPlus wantsCount (Loadbalance deadRouteMapRef)) = do
  routeMap <- getRouteMapRef routeMapName
  return $ ActionPlus wantsCount (Loadbalance routeMap)
    where routeMapName = getRefName deadRouteMapRef
instantiateAction action =
  return action

runPolicies :: (?env::Env) => IO (Async())
runPolicies =
    async $ monitor "Policies" [policyKeyPrefix] policyChanged policyDeleted

impossible :: String -> a
impossible = throw . InternalError . (++ "Impossible condition: ")

policyNameFrom :: ByteString -> ByteString
policyNameFrom key = fromMaybe badKey $ stripPrefix policyKeyPrefix key
      where badKey = impossible $ "routing map change on malformed key " ++ unpack key

policyChanged :: (?env::Env, MonadIO m, Failable m) => ByteString -> m ()
policyChanged = void . importPolicy db
  where db = getDB ?env

policyDeleted :: (?env::Env, Failable m, MonadIO m) => ByteString -> m ()
policyDeleted  = void . importPolicy db
  where db = getDB ?env

importPolicy :: (Adapter a, Failable m, MonadIO m) => a -> ByteString -> m PolicyRef
importPolicy db key = do
  entries <- getMap db key
  case entries of
    [] ->
      deletePolicy name >> getPolicyRef name
    _  ->
      readPolicy db name >>= updatePolicy name
  where name = policyNameFrom key
