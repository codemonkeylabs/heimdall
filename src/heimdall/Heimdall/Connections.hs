{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes      #-}

module Heimdall.Connections where

import Control.Arrow                ((&&&))
import Control.Lens
import Control.Monad.Failable       (Failable, hoist)
import Control.Monad.State.Strict   (StateT)
import Data.ByteString              (ByteString)
import Data.Count                   (Field, Tag)
import Data.Default                 (Default(..))
import Data.IP                      (IP)
import Data.Print                   (Print(..))
import Heimdall.Exceptions
import Heimdall.Packet
import Heimdall.Stats
import Heimdall.Types
import Network.Flow                 (Direction(..))

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Lazy.Char8       as L

data Connection = Connection { _source        :: Maybe (VRFID, IP),
                               _flowId        :: Int,
                               _rpIDB         :: Maybe IDB,
                               _startTime     :: L.ByteString,
                               _startHash     :: Int,
                               _verdict       :: Bidirectional [ActionPlus],
                               _final         :: Bidirectional Result,
                               _logging       :: Bidirectional Bool,
                               _statistics    :: Bidirectional (Maybe ([Field StatsField Counter],[Tag IndexField L.ByteString]))
                             }

type Bidirectional a = (a, a)

bi :: a -> (a, a)
bi = id &&& id

upwards :: Field1 a a b b => Lens' a b
upwards   = _1

downwards :: Field2 a a b b => Lens' a b
downwards = _2

data AnalState = AnalState {
                    _buffer    :: ByteString,
                    _direction :: Maybe Direction,
                    _depth     :: FilterDepth,
                    _continue  :: Maybe (ByteString -> A.Result L457Analysis),
                    _analysis  :: L457Analysis
                 }

instance Default AnalState where
    def = AnalState { _buffer    = mempty,
                      _depth     = ApplicationLayer,
                      _direction = Nothing,
                      _continue  = Nothing,
                      _analysis  = def }

instance Print Connection where
    toString Connection {..} =
        sourceIP ++ ' ':
        "RP: " ++ toString _rpIDB
            where sourceIP = maybe "<new>" show _source

type AnalST = StateT AnalState

getDirection :: (Failable m) => Packet -> Connection -> m Direction
getDirection Packet {..} Connection {..} = do
  connectionSrc <- hoist (const $ InternalError "Improperly setup connection") _source
  case connectionSrc of
    (_, ip) | getSrcIP == ip ->
      return Upwards
    _ ->
      return Downwards

makeLenses ''AnalState
makeLenses ''Connection

getVRFID :: (Failable m) => Connection -> m VRFID
getVRFID = fmap (view _1) . hoist (const $ InternalError "Missing connection VRF ID") . view source
