{-# LANGUAGE OverloadedStrings #-}
{- |
Description: H.E.I.M.D.A.L.L - Highly Effective Intervention Manipulation and Data Analysis at the
                               Link Layer
Copyright: (c) Erick Gonzalez, 2018
License: LGPL (See LICENSE)
Maintainer: erick@codemonkeylabs.com

Passively monitors data flows according to configured rules, triggering intervention
actions to allow other network components to intercept or reroute a connection as needed.

-}
module Heimdall where

import Control.Concurrent           (ThreadId, myThreadId, threadDelay)
import Control.Concurrent.Async     (Async, waitAnyCatchCancel)
import Control.Concurrent.MVar      (putMVar)
import Control.Exception            (Exception (..), SomeException, catch, handle, throwTo)
import Control.Monad                (forM, forever, void)
import Control.Monad.Fix            (fix)
import Control.Monad.Trans          (lift)
import Control.Monad.Trans.Writer   (WriterT (..), execWriterT, tell)
import Data.Maybe                   (fromMaybe)
import Data.Text                    (Text)
import Database.Adapter             (new)
import Heimdall.ACLs                (runACLs)
import Heimdall.Connection.Tracking (runTracking)
import Heimdall.Environment
import Heimdall.Interface.Queues    (runQueues)
import Heimdall.Interface.Tunnels   (runTunnels)
import Heimdall.Interface.VIFs      (runVIFs)
import Heimdall.Policies            (runPolicies)
import Heimdall.RouteMaps           (runRouteMaps)
import Heimdall.Stats               (runStats)
import Heimdall.Time        as Time (keeper)
import Options.Applicative
import System.Exit                  (exitSuccess)
import System.Posix.Signals         (Handler (..), Signal, SignalInfo (..),
                                     installHandler, sigINT, sigQUIT, sigTERM)
import Utils.Ini                    (getIniOpts)

import qualified Database.Redis               as R
import qualified System.Logging               as L

data ExitException = ExitException Signal
  deriving Show

instance Exception ExitException

data Options = Options {
                 iniFilePath :: Maybe String
               , nodeName    :: String
               , redisConf   :: R.ConnectInfo
               }

signalsHandler :: ThreadId -> SignalInfo -> IO ()
signalsHandler mainTId inf = do
  throwTo mainTId $ ExitException $ siginfoSignal inf

preParseCmdLine :: IO Options
preParseCmdLine = parseCmdLine
  where ?defOptions = defaultOptions

-- | Entry point for the Heimdall service
runService :: IO ()
runService = do
  mainTId <- myThreadId
  -- sigkill cannot be intecepted
  void $ installHandler sigINT  (CatchInfoOnce $ signalsHandler mainTId) Nothing
  void $ installHandler sigQUIT (CatchInfoOnce $ signalsHandler mainTId) Nothing
  void $ installHandler sigTERM (CatchInfoOnce $ signalsHandler mainTId) Nothing
  Options { iniFilePath } <- preParseCmdLine
  iniOpts <- forM iniFilePath $ flip (getIniOpts "Global") mempty
  let ?defOptions = fromMaybe mempty iniOpts
  options <- parseCmdLine
  let redisOpts = redisConf options
  putMVar redisConfigVar redisOpts
  db      <- fix $ retry (new redisOpts) "Failure to create DB adapter: "
  forever $ do
    let ?env = Env { getDB   = db,
                     getNode = nodeName options}
    monitors <- execWriterT $ do
      spawn $ L.monitor db
      spawn runTracking
      spawn runStats
      spawn runPolicies
      spawn runRouteMaps
      spawn runQueues
      spawn runTunnels
      spawn runVIFs
      spawn runACLs
      spawn Time.keeper
    handle (\(ExitException signal) -> exitBySignal signal) $ do
      s <- (snd <$> waitAnyCatchCancel monitors)
      L.errorM "Heimdall" $ "thread exits with:" ++ show s ++ " Restart."
    where retry f msg loop = f `catch` \(e::SomeException) -> do
               L.errorM "Heimdall" $ msg ++ show e
               threadDelay 2718281
               loop
          exitBySignal signal = do
            L.infoM "Heimdall" $ "Exit by signal: " <> show signal
            exitSuccess

spawn :: IO (Async ()) -> WriterT [Async ()] IO ()
spawn fn = lift fn >>= tell . pure

parseCmdLine :: (?defOptions :: [(Text, Text)]) => IO Options
parseCmdLine = execParser opts
  where
    opts = info (parseOptions <**> helper)
           ( fullDesc
          <> header "heimdall - Highly Effective Intervention Manipulation and Data Analysis at the Link Layer"
           )

parseOptions :: (?defOptions :: [(Text, Text)]) => Parser Options
parseOptions =
  Options <$> optional (confOption "ini-file" "FILENAME" "INI file location")
          <*> confOption "node" "NODE" "Local node name"
          <*> redisOptions
