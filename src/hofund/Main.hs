{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Hofund - Heimdall CLI
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Main where

import Control.Monad                (forM)
import Control.Monad.Except         (runExceptT)
import Control.Monad.State.Strict   (evalStateT)
import Data.Default                 (def)
import Data.Text                    (Text)
import Data.Maybe                   (fromMaybe)
import Database.Adapter             (new)
import Heimdall.Environment
import Hofund.Commands              (root)
import Hofund.Types                 (AppState (..))
import Revision                     (getRevTH)
import System.Console.StructuredCLI hiding (Parser)
import Utils.Ini                    (getIniOpts)

import qualified Database.Redis               as R
import           Options.Applicative

data Options = Options {
                 batch       :: Bool
               , iniFilePath :: Maybe String
               , redisConf   :: R.ConnectInfo
               }

preParseCmdLine :: IO Options
preParseCmdLine = parseCmdLine
  where ?defOptions = defaultOptions

main :: IO ()
main = do
  Options { iniFilePath } <- preParseCmdLine
  iniOpts <- forM iniFilePath $ flip (getIniOpts "Global") mempty
  let ?defOptions = fromMaybe mempty iniOpts
  Options {batch, redisConf} <- parseCmdLine
  db <- runExceptT (new redisConf) >>= either dbError return
  result <- evalStateT (runCLI "Heimdall" settings { isBatch = batch } root) $
              AppState db "" "" "" False False False
  case result of
    Left Exit ->
        return ()
    Left e ->
        putStrLn $ "Unrecoverable exception: " ++ show e
    _ ->
        return ()
    where settings = def { getBanner = "Heimdall CLI\nTab completion is your friend. "
                                         ++ "Question mark (?) is your saviour.",
                           getHistory = Just ".hofund.history" }
          dbError err =
            error $ "Failed to connect to DB: " ++ show err

versioner :: Parser (a -> a)
versioner = infoOption $(getRevTH) (long "version" <> help "Show the version" <> hidden)

parseCmdLine :: (?defOptions :: [(Text, Text)]) => IO Options
parseCmdLine = execParser opts
  where
    opts = info (parseOptions <**> helper <**> versioner)
           ( fullDesc
          <> header "heimdall CLI"
           )

parseOptions :: (?defOptions :: [(Text, Text)]) => Parser Options
parseOptions =
  Options <$> switch (long "batch" <>  short 'b' <> help "Enable batch processing mode")
          <*> optional (confOption "ini-file" "FILENAME" "INI file path")
          <*> redisOptions
