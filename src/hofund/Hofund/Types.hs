{-# LANGUAGE ExistentialQuantification, FlexibleInstances, TypeSynonymInstances #-}

module Hofund.Types where

import Data.ByteString              (ByteString)
import Control.Monad.State.Strict   (StateT)
import Database.Adapter.Redis       (Redis)
import System.Console.StructuredCLI (CommandsT)

data AppState = AppState { db       :: Redis,
                           target   :: ByteString,
                           arg1     :: ByteString,
                           arg2     :: ByteString,
                           inTunnel :: Bool,
                           inQueue  :: Bool,
                           inVIF    :: Bool
                         }

type StateM = StateT AppState IO

type Commands = CommandsT StateM ()
