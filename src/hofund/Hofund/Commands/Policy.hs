{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.Policy where

import Control.Monad                (void)
import Control.Monad.Except         (runExceptT)
import Control.Monad.IO.Class       (liftIO)
import Control.Monad.State.Strict   (gets)
import Control.Monad.Trans          (lift)
import Control.Monad.Trans.Maybe    (runMaybeT)
import Data.ByteString.Char8        (empty, pack)
import Data.Configurable            (deserialize, serialize)
import Data.Maybe                   (fromMaybe)
import Data.Referable               (deadRef)
import Database.Adapter             (del, delMapKVs, getMapKV, setMapKVs)
import Heimdall.Types
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Action, Commands)

import qualified System.Console.StructuredCLI     as CLI

policy :: Commands
policy =
  custom "policy" "Configure a traffic policy" parsePolicyName always [] setTarget >+ do
    basic
    command "add" "Add entry to a routing policy" newLevel >+ do
           addRule
           basic
    command "remove" "Remove entry from a routing policy" newLevel >+ do
           basic
           removeRule
    showPolicy
    destroy

addRule :: Commands
addRule =
  custom "rule" "<integer rule priority>" parseACLRule always [] setACE >+ do
      basic
      continue'
      discard
      routeVia
      loadbalance
      command "show" "Display currently configured actions upon matching ACL in this policy" $ do
        printOutput $ putStrLn "Not yet implemented"
        return NoAction
    where setACE (priority, ace) = do
            void $ setArg1 priority
            void . setArg2 $ show ace
            return NewLevel

removeRule :: Commands
removeRule =
  param "rule" "<rule priority>" asInt $ \priority -> do
    name <- getTarget
    db   <- gets db
    str  <- serialize priority
    let key = policyKeyPrefix <> name
    delMapKVs db key [str]
    return NoAction

routeViaCmd :: Handler StateM IntfName -> Commands
routeViaCmd = custom "route-via" "<interface name to route traffic out on>" parseIntf always []

routeVia :: Commands
routeVia = routeViaCmd $ setRuleAction . RouteVia . deadRef

loadbalanceCmd :: Handler StateM String -> Commands
loadbalanceCmd = custom "loadbalance" hint (parseNameFor routeMapKeyPrefix hint) always []
                    where hint = "Perform traffic loadbalancing over a route-map"

loadbalance :: Commands
loadbalance = loadbalanceCmd $ setRuleAction . Loadbalance . deadRef . pack

discard :: Commands
discard = command "discard" "Drop traffic" $ setRuleAction Discard

continue' :: Commands
continue' = command "continue" "Forward traffic over the network" $ setRuleAction Continue

setRuleAction :: Action -> StateM CLI.Action
setRuleAction action = do
  result <- runExceptT $ do
    policyName <- lift getTarget
    priority   <- lift getArg1
    ace        <- lift getArg2
    db         <- gets db
    let key = policyKeyPrefix <> policyName
    (_, actions) <- fromMaybe (empty, []) <$> runMaybeT (deserialize =<< getMapKV db key priority)
    let actions' = reverse actions
    str <- serialize (ace, reverse $ action : actions')
    setMapKVs db key [(priority, str)]
  either reportFailure return result
  return NoAction
      where reportFailure e =
              liftIO . putStrLn $ "failure to set rule action " ++ show action ++ ": "
                ++ show e

showPolicies :: Commands
showPolicies =
    command "policies" "Show configured traffic policies" $ do
      showObjects policyKeyPrefix
      return NoAction

showPolicy ::  Commands
showPolicy =
    command "show" "Display traffic policy contents" $ do
      policyName <- getTarget
      showObject (policyKeyPrefix <> policyName) Nothing
      return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this policy from the system" $ do
    db <- gets db
    policyName <- getTarget
    del db [policyKeyPrefix <> policyName]
    return NoAction
