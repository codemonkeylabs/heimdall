{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.Tracking where

import Control.Monad.State.Strict   (gets)
import Data.ByteString.Char8        (ByteString)
import Data.Configurable            (serialize)
import Database.Adapter
import Heimdall.Types.Connection    (connTrackKeyPrefix)
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Commands)


ttlKey :: ByteString
ttlKey = connTrackKeyPrefix <> "ttl"

maxSizeKey :: ByteString
maxSizeKey = connTrackKeyPrefix <> "max-size"

gcLifeTimesKey :: ByteString
gcLifeTimesKey = connTrackKeyPrefix <> "gc-lifetimes"

tracking :: Commands
tracking =
    command "tracking" "Connection tracking configuration" newLevel >+ do
      basic
      command "set" "Set configuration parameter" newLevel >+ do
        basic
        param "ttl" "Time to live (in milliseconds) for inactive connections" asInt $ \ttl -> do
          db <- gets db
          set db ttlKey =<< serialize ttl
          return NoAction
        param "max-size" "Maximum size of connection tracking table" asInt $ \maxSize -> do
          db <- gets db
          set db maxSizeKey =<< serialize maxSize
          return NoAction
        param "gc-lifetimes" "TTL lifetimes to wait between garbage collections" asInt $ \n -> do
          db <- gets db
          set db gcLifeTimesKey =<< serialize n
          return NoAction
      command "delete" "Delete a configuration parameter" newLevel >+ do
        basic
        command "ttl" "Remove TTL configuration" $ do
          db <- gets db
          del db [ttlKey]
          return NoAction
        command "max-size" "Remove maximum size configuration" $ do
          db <- gets db
          del db [maxSizeKey]
          return NoAction
        command "gc-lifetimes" "Remove garbage collection lifetime configuration" $ do
          db <- gets db
          del db [gcLifeTimesKey]
          return NoAction