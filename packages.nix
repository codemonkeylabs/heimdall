#
{dev, haskellLib, ...}: self: super: let

in {
  mkDerivation = args : super.mkDerivation (args // {
    doCheck   = false;
    doHaddock = false;
    enableLibraryProfiling = dev;
  });

  ghc = super.ghc;

  heimdall = self.callCabal2nix "heimdall" (
               builtins.filterSource (path: type: type != "directory" ||
                                      baseNameOf path != ".git") ./. ) { };

  # overriden nixpkgs packages we need...

  n-core = self.callCabal2nix "n-core" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/n-core";
      ref    = "stable"; }
    ) {};

  the-count = self.callCabal2nix "the-count" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/the-count";
      ref    = "master";
    }) {};

  failable = self.callCabal2nix "failable" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/failable";
      ref    = "master";
    }) {};

  not-prelude = self.callCabal2nix "not-prelude" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/not-prelude";
      ref    = "master";
    }) {};

  structured-cli = self.callCabal2nix "structured-cli" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/structured-cli";
      rev    = "3507e4028601d7dbfcc0bb66022921a77a4bb3be"; }
    ) {};

  network-packet-linux = haskellLib.markUnbroken super.network-packet-linux;
}
