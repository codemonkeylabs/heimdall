{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Interfaces Module
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Heimdall.Environment where

import Control.Concurrent.MVar
import Control.Monad.Except       (runExcept)
import Control.Monad.Failable     (hoist)
import Control.Monad.IO.Class
import Control.Monad.Trans.Reader (runReaderT)
import Data.Configurable
import Data.Maybe                 (fromMaybe)
import Data.Text
import Data.Word                  (Word16)
import Database.Adapter.Redis     (Redis)
import Options.Applicative
import Options.Applicative.Types  (ReadM(..))
import System.IO.Unsafe

import qualified Data.ByteString.Char8   as B
import qualified Database.Redis          as R

data Env = Env { getDB   :: Redis,
                 getNode :: !String }

reader :: Configurable a => ReadM a
reader = maybeReader $ deserialize . B.pack

parseWith :: ReadM a -> String -> Maybe a
parseWith (ReadM f) = hoist (const ()) . runExcept . runReaderT f

confOption :: (?defOptions :: [(Text, Text)], Configurable a) => String
                                                              -> String
                                                              -> String
                                                              -> Parser a
confOption name var desc =
  option reader $ long name
                <> metavar var
                <> help desc
                <> defValue
    where key      = pack name
          defValue = maybe mempty value  $
                      parseWith reader . unpack =<< lookup key ?defOptions

-- Note: We need that for the FFI to be able to create redis connection from there.

{-# NOINLINE redisConfigVar #-}
redisConfigVar :: MVar R.ConnectInfo
redisConfigVar = unsafePerformIO newEmptyMVar

readRedisConfig :: MonadIO m => m R.ConnectInfo
readRedisConfig = liftIO . readMVar $ redisConfigVar

redisOptions :: (?defOptions :: [(Text, Text)]) => Parser R.ConnectInfo
redisOptions = toConnectInfo <$> parseRedisOptions'
  where
    parseRedisOptions' = (,,)
      <$> optional (confOption "redis-host" "HOST" "Redis DB host")
      <*> optional (confOption "redis-port" "PORT" "Redis server port number")
      <*> optional (confOption "redis-pass" "PASSWORD" "Redis DB password")
    toConnectInfo (hostName, portNumber :: Maybe Word16, password) = R.defaultConnectInfo
      { R.connectHost = fromMaybe "localhost" hostName
      , R.connectPort = R.PortNumber . fromIntegral . fromMaybe 6379 $ portNumber
      , R.connectAuth = password
      }

defaultOptions :: [(Text, Text)]
defaultOptions = mempty
