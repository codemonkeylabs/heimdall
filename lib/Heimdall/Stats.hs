{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeApplications  #-}

module Heimdall.Stats (Counter,
                       StatsField(..),
                       IndexField(..),
                       StatsCount,
                       context,
                       runStats,
                       withContext) where

import Control.Concurrent          (threadDelay)
import Control.Concurrent.Async    (Async, AsyncCancelled, async, cancel)
import Control.Exception           (Handler(..), SomeException, catches, throw)
import Control.Monad               (forM_)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (failure, failableIO)
import Control.Monad.Fix           (fix)
import Control.Monad.IO.Class      (MonadIO, liftIO)
import Control.Lens                ((<<.=), (?=), makeLenses, use)
import Control.Monad.State.Strict  (runState)
import Data.ByteString.Char8       (ByteString, unpack)
import Data.Configurable           (deserialize)
import Data.Count                  (Count, FieldIndex)
import Data.Default                (def)
import Data.Fn                     ((.-.))
import Data.IORef                  (IORef, atomicModifyIORef', newIORef)
import Data.IP                     (IP(..), fromHostAddress, fromHostAddress6)
import Data.Ix                     (Ix)
import Data.Tuple                  (swap)
import Network.Address             (Error(..))
import Heimdall.Environment
import Heimdall.ObjectModel
import Network.Socket              (SockAddr(..), Family(..), PortNumber)
import Service.Influx              (createService, runService)
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging       hiding (monitor)

import qualified Data.ByteString.Lazy.Char8 as L
import qualified Control.Monad.State.Strict as SS
import qualified Database.Adapter           as DB
import qualified Service.Influx             as Influx

statsKeyPrefix :: ByteString
statsKeyPrefix = "config.heimdall.stats."

data StatsField = RxPackets
                | RxBytes
                | TxPackets
                | TxBytes
                | RoutedPackets
                | DeflectedPackets
                deriving (Eq, Ix, Ord, Bounded, Enum, Show)

instance Influx.Key StatsField where
  showKey DeflectedPackets = "deflected"
  showKey RoutedPackets    = "routed"
  showKey RxBytes          = "rx-bytes"
  showKey RxPackets        = "rx-pkts"
  showKey TxBytes          = "tx-bytes"
  showKey TxPackets        = "tx-pkts"

instance Influx.Key IndexField where
  showKey ActionField     = "action"
  showKey CommentField    = "comments"
  showKey FlowField       = "flow"
  showKey FlowStart       = "start"
  showKey InterfaceField  = "intf"
  showKey LeftIP          = "left-ip"
  showKey LeftPortField   = "left-port"
  showKey PolicyField     = "policy"
  showKey ProtocolField   = "protocol"
  showKey RightIP         = "right-ip"
  showKey RightPortField  = "right-port"
  showKey TimeField       = "timestamp"
  showKey TypeField       = "type"
  showKey VRFField        = "vrf"

instance FieldIndex StatsField

type StatsCount   = Count StatsField Counter IndexField L.ByteString

data IndexField = ActionField
                | CommentField
                | FlowField
                | FlowStart
                | InterfaceField
                | LeftIP
                | LeftPortField
                | PolicyField
                | ProtocolField
                | RightIP
                | RightPortField
                | TimeField
                | TypeField
                | VRFField
                deriving (Eq, Ord, Enum, Show)

type Counter = Int

data State = State { _serviceThread :: Maybe (Async ()),
                     _context       :: Maybe Context }

type Context = Influx.Context StatsField Counter IndexField L.ByteString

makeLenses ''State

{-# NOINLINE state #-}
state :: IORef State
state = unsafePerformIO $ newIORef State { _serviceThread = Nothing,
                                           _context       = Nothing }

withState :: (MonadIO m) => SS.State State a -> m a
withState = liftIO . atomicModifyIORef' state . swap .-. runState

runStats :: (?env::Env) => IO (Async())
runStats =
    async $ monitor "Stats" [statsKeyPrefix] configChange configChange

configChange :: (?env::Env) => ByteString -> IO ()
configChange key@"config.heimdall.stats.influx-apn" = do
    apn <- DB.lookup db key
    maybe stopService startService apn
  where db = getDB ?env
configChange key = do
  infoM "Stats" $ "Ignoring " ++ unpack key
  return ()

stopService :: IO ()
stopService = do
    infoM "Stats" "Stopping Influx statistics collection"
    thread <- withState $ serviceThread <<.= Nothing
    forM_ thread cancel

getIPAddress :: SockAddr -> IP
getIPAddress (SockAddrInet _ addr)      = IPv4 $ fromHostAddress addr
getIPAddress (SockAddrInet6 _ _ addr _) = IPv6 $ fromHostAddress6 addr
getIPAddress _                          = throw $ UnsupportedAddressFamily AF_UNIX

getPort :: SockAddr -> PortNumber
getPort (SockAddrInet port _)      = port
getPort (SockAddrInet6 port _ _ _) = port
getPort _                          = throw $ UnsupportedAddressFamily AF_UNIX

startService :: ByteString -> IO ()
startService apn = do
  stopService
  result <- runExceptT $ do
    sockAddr <- deserialize apn
    let ip   = getIPAddress sockAddr
        port = getPort sockAddr
        config = def { Influx.getHostIP   = ip,
                       Influx.getHostPort = port }
    ctxt  <- createService "heimdall-stats" config
    withState  (context ?= ctxt)
    failableIO . async . fix $ serviceLoop ctxt
  either failure saveThread result
    where saveThread               = withState . (serviceThread ?=)
          serviceLoop _context loop = do
            runService _context `catches` [Handler uponCancel, Handler uponError]
            threadDelay 2718281
            loop
          uponCancel (e::AsyncCancelled) = do
            infoM "Stats" $ "Influx service has been canceled"
            throw e
          uponError (e::SomeException) =
            infoM "Stats" $ "Influx service has been terminated upon " ++ show e

withContext :: MonadIO m => m (Maybe Context)
withContext = withState (use context)
