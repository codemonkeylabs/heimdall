{-# LANGUAGE PackageImports #-}

module Heimdall.Types.Flow where

import Network.Flow
import Heimdall.Packet    (Packet(..))

getFlow :: Packet -> Flow
getFlow Packet{..} = flowFor getSrcIP (fromIntegral getSrcPort) getDstIP (fromIntegral getDstPort) getProto
