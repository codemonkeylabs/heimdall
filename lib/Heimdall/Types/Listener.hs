{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.Listener where

import Control.Lens                  (_2, makeLenses, view)
import Control.Concurrent.Async      (Async)
import Control.Exception             (throw)
import Data.ByteString.Char8         (unpack)
import Data.Configurable             (serialize)
import Data.IP                       (IP)
import Data.Map.Strict               (Map)
import Data.Maybe                    (fromMaybe)
import Data.Print                    (Print(..))
import Data.Referable                (Referable, VolatileRef)
import Heimdall.Common.Encapsulation
import Heimdall.Exceptions
import Heimdall.Types.Network

type ListenerName = (Encapsulation, IP)

listenerAddressFrom :: ListenerName -> IP
listenerAddressFrom = view _2

instance Print ListenerName where
    toString (encapsulation, address) = encapName ++ '/':show address
        where encapName  = unpack . fromMaybe impossible $ serialize encapsulation
              impossible = throw . InternalError $ "serializing " ++ show encapsulation
                             ++ " for listener on " ++ show address

data Listener a = Listener { _listenerName     :: ListenerName,
                             _listenerUsers    :: Int,
                             _listenerThread   :: Maybe (Async ()),
                             _listenerVRFTable :: Map VRFID a }

instance Referable (Listener a)

type ListenerRef a = VolatileRef ListenerName (Listener a)

makeLenses 'Listener