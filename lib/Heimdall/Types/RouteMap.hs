{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Types.RouteMap where

import Data.ByteString               (ByteString)
import Data.IntMap                   (IntMap)
import Data.IP                       (IPv4, IPv6)
import Data.IP.RouteTable            (IPRTable)
import Data.Referable                (Referable, VolatileRef)
import Data.Vector                   (Vector)

type RouteMapName = ByteString

type RouteMapTable_ idb a = IPRTable a (Vector (idb, ByteString))

type RouteMapTables_ idb = (RouteMapTable_ idb IPv4, RouteMapTable_ idb IPv6)

data RouteMap_ idb = RouteMap { routeMapName   :: RouteMapName,
                                routeMapTables :: IntMap (RouteMapTables_ idb) }

instance Referable (RouteMap_ idb)

type RouteMapRef_ idb = VolatileRef RouteMapName (RouteMap_ idb)

routeMapKeyPrefix :: ByteString
routeMapKeyPrefix = "config.heimdall.route-map."
