{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Heimdall.Types.Interface where

import Control.Applicative           ((<|>))
import Control.Arrow                 ((>>>))
import Control.Concurrent.Async      (Async)
import Control.Lens
import Control.Monad                 (void)
import Control.Monad.Failable
import Data.Bifunctor                (first)
import Data.ByteString.Builder       (toLazyByteString, byteString, char8, string8, intDec)
import Data.ByteString.Char8         (ByteString, spanEnd, pack, unpack)
import Data.ByteString.Lazy          (toStrict)
import Data.Configurable             (Configurable(..))
import Data.IORef                    (IORef)
import Data.Print                    (Print, toString)
import Data.Referable                (Referable, VolatileRef)
import Data.Set                      (Set)
import Heimdall.Exceptions
import Heimdall.Packet
import Heimdall.Stats
import Heimdall.Types.Connection
import Heimdall.Types.Network
import Text.Read                     (readMaybe)

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.Set as Set

-- | Defines name of the interface
data IntfName = IntfName { _intfType :: IntfType ,
                         -- ^ Type of the interface
                           _intfSub  :: Int      ,
                         -- ^ Interface subsection or subspace. Defined by a user to distinguish
                         -- different kinds of interfaces client and internal for example.
                           _intfId   :: Int
                         -- ^ Interface ID 
                         }
    deriving (Eq, Ord)

data IntfType = TunnelIntf
              | QueueIntf
              | VIFIntf
                deriving (Eq, Ord, Enum, Bounded)

instance Show IntfType where
    showsPrec _ TunnelIntf = ("tunnel" ++)
    showsPrec _ QueueIntf  = ("queue" ++)
    showsPrec _ VIFIntf = ("vif" ++)

instance Read IntfType where
    readsPrec _ "queue"  = [(QueueIntf, "")]
    readsPrec _ "vif"    = [(VIFIntf, "")]
    readsPrec _ "tunnel" = [(TunnelIntf, "")]
    readsPrec _ _        = []

instance Print IntfName where
    toString = show

intfKeyPrefix :: ByteString
intfKeyPrefix = "config.heimdall."

instance Show IntfName where
    showsPrec _ (IntfName typ sub iD) = ((show typ ++ '/':show sub ++ '/':show iD) ++)

instance Configurable IntfName where
  serialize   = return . pack . show
  deserialize = either (failure . InvalidValue) return . A.parseOnly parseIntfName

parseIntfName :: A.Parser IntfName
parseIntfName = do
  typ <- assert =<< intfType
  void $ A.char '/'
  sub <- assert =<< digits
  void $ A.char '/'
  iD <- assert =<< digits
  return $ IntfName typ sub iD
    where assert   = maybe (fail "Error parsing interface name") return
          digits   = readMaybe <$> A.many1 (A.satisfy A.isDigit)
          intfType = fmap (readMaybe . unpack) . A.choice $ fmap A.string allTypes
          allTypes = pack.show <$> ([minBound..maxBound] :: [IntfType])

getIntfNameFromKey :: ByteString -> Maybe IntfName
getIntfNameFromKey = parse . first getType . splitDot
    where splitDot = spanEnd (/= '.')
          dropDot  = spanEnd (== '.')
          getType  = fst . dropDot >>> snd . splitDot
          parse    = hoist InvalidValue . A.parseOnly parseIntfName .
                       mconcat . toListOf each . over _2 (mappend "/")

getIntfRootKey :: ByteString -> Maybe (IntfName, ByteString)
getIntfRootKey ""  = Nothing
getIntfRootKey key =
    case getIntfNameFromKey key of
      Just name ->
        return (name, key)
      Nothing ->
        getIntfRootKey . fst . spanEnd (== '.') . fst $ spanEnd (/= '.') key

mkKeyFromIntfName :: IntfName -> ByteString
mkKeyFromIntfName i = toStrict . toLazyByteString
                    $ byteString intfKeyPrefix
                    <> string8 (show $ _intfType i)
                    <> char8 '.'
                    <> intDec (_intfSub i)
                    <> char8 '/'
                    <> intDec (_intfId i)

data Interface_ acl policyMap = Interface { _intfName         :: IntfName,
                                            _intfVRFId        :: VRFID,
                                            _intfPolicyMap    :: policyMap,
                                            _intfCapabilities :: Capabilities,
                                            _intfDeflector    :: Maybe (IDB_ acl policyMap),
                                            _intfConnTag      :: Maybe ConnectionTag,
                                            _intfMasquerading :: Bool,
                                            _intfRP           :: Maybe (IDB_ acl policyMap),
                                            _intfLogACL       :: Maybe acl,
                                            _intfTrackStats   :: Bool,
                                            _intfCounter      :: IORef (Maybe StatsCount),
                                            _intfForwarder    :: Forwarder_ acl policyMap }

data ICB_ acl policyMap = ICB { _icbIntf   :: Maybe (Interface_ acl policyMap),
                                _icbThread :: Async (),
                                _icbInfo   :: [(ByteString,ByteString)] }

instance Referable (ICB_ acl policyMap)

type IDB_ acl policyMap = VolatileRef IntfName (ICB_ acl policyMap)

type Forwarder_ acl policyMap = Packet -> Interface_ acl policyMap -> IO ()

data XmitDirection = Rx | Tx deriving Show

data Capability = RxData
                | TxData
                  deriving (Eq, Ord)

can :: Capability -> Capabilities
can = Capabilities . Set.singleton

(<?>) :: Interface_ acl policyMap -> Capabilities -> Bool
(<?>) Interface{..} (Capabilities setA) = canAllOf _intfCapabilities
    where canAllOf (Capabilities setB) = setA `Set.isSubsetOf` setB

newtype Capabilities = Capabilities (Set Capability) deriving (Semigroup, Monoid)

data ServiceType = IngressService
                 | EgressService
                 | ForwardingService
                   deriving (Bounded, Enum)

instance Configurable ServiceType where
    serialize IngressService    = return "ingress"
    serialize EgressService     = return "egress"
    serialize ForwardingService = return "forwarding"
    deserialize = either (failure . InvalidValue) return . A.parseOnly serviceTypeParser
        where serviceTypeParser = ingressServiceType <|>
                                  egressServiceType  <|>
                                  forwardingServiceType

ingressServiceType :: A.Parser ServiceType
ingressServiceType = do
  void $ A.string "ingress"
  return IngressService

egressServiceType :: A.Parser ServiceType
egressServiceType = do
  void $ A.string "egress"
  return EgressService

forwardingServiceType :: A.Parser ServiceType
forwardingServiceType = do
  void $ A.string "forwarding"
  return ForwardingService

makeLenses ''Interface_
makeLenses ''ICB_
makeLenses ''IntfName

