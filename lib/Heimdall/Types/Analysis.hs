{-# LANGUAGE GADTs             #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.Analysis where

import Control.Lens    (makeLenses)
import Data.Default    (Default, def)
import Data.ByteString (ByteString)
import Network.Flow    (Flow)

data Status = Undecisive
            | Incomplete
            | Partial
            | Finished
            | Failed
            deriving Show

data ApplicationProtocol = UnknownProtocol
                         | HTTP
                         | TLS
                         deriving Show

type HTTPHeader = (ByteString, ByteString)

class IsAProtocol p

data AppMetadata a where
    DummyData :: AppMetadata a
    HTTPInfo  :: { _method          :: ByteString,
                   _uri             :: ByteString,
                   _version         :: ByteString,
                   _requestHeaders  :: [HTTPHeader],
                   _responseHeaders :: [HTTPHeader],
                   _responseStatus  :: Maybe (ByteString, ByteString)
                  } -> AppMetadata a
    TLSInfo   :: { sni    :: Maybe ByteString } -> AppMetadata a
    deriving Show

data L457Analysis = L457Analysis {
                        _status   :: Status,
                        _analFlow :: Maybe Flow,
                        _protocol :: ApplicationProtocol,
                        _metadata :: AppMetadata ApplicationProtocol
                    }
                   deriving Show

instance Default L457Analysis where
    def = L457Analysis {
              _status   = Incomplete,
              _analFlow = Nothing,
              _protocol = UnknownProtocol,
              _metadata = DummyData
          }

makeLenses ''L457Analysis
makeLenses ''AppMetadata

