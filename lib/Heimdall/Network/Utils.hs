module Heimdall.Network.Utils where

import Data.IP              (IP(..), IPRange(..))
import Network.Socket       (Family(..))


rangeFamily :: IPRange -> Family
rangeFamily (IPv4Range _) = AF_INET
rangeFamily (IPv6Range _) = AF_INET6

addressFamily :: IP -> Family
addressFamily (IPv4 _) = AF_INET
addressFamily (IPv6 _) = AF_INET6
