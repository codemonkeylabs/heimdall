module Heimdall.Common.ObjectModel where

import Data.ByteString.Char8        (ByteString, spanEnd)

getNameFromKey :: ByteString -> ByteString
getNameFromKey = snd . spanEnd (/= '.')
