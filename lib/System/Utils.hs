module System.Utils where

import Control.Concurrent.MVar (MVar, newMVar, takeMVar, putMVar)
import Control.Exception       (bracket_, bracketOnError)
import Control.Monad.IO.Class  (MonadIO, liftIO)
import Data.Fn                 ((.-.))
import Data.Int                (Int64)
import System.Clock            (Clock(Monotonic), TimeSpec(..), getTime)
import System.Exit             (ExitCode(..))
import System.Process          (runCommand, terminateProcess, waitForProcess)
import System.Timeout          (timeout)
import System.IO.Unsafe        (unsafePerformIO)

timeSinceStart :: MonadIO m => m Int64
timeSinceStart = liftIO $
    getTime Monotonic >>= \(TimeSpec secs ns) ->
        return $ secs * 1000000000 + ns

{-# NOINLINE execMutex #-}
execMutex :: MVar ()
execMutex = unsafePerformIO $ newMVar ()

execExclusively :: (MonadIO m) => Int -> String -> m (Maybe ExitCode)
execExclusively =
  exclusively .-. execCommandWithTimeout
    where lock        = takeMVar execMutex
          unlock      = putMVar execMutex ()
          exclusively = liftIO . bracket_ lock unlock

execCommandWithTimeout :: (MonadIO m) => Int -> String -> m (Maybe ExitCode)
execCommandWithTimeout microSecs cmd =
    liftIO $ bracketOnError runCmd terminateCmd $ timeout microSecs . waitForProcess
        where runCmd          = runCommand cmd
              terminateCmd pH = terminateProcess pH >> waitForProcess pH

execCommand :: (MonadIO m) => String -> m (Maybe ExitCode)
execCommand = execCommandWithTimeout maxBound
