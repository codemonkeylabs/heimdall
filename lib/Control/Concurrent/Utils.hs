module Control.Concurrent.Utils where

import Control.Concurrent           (throwTo, threadDelay)
import Control.Concurrent.Async     (Async, AsyncCancelled(..), asyncThreadId, poll)
import Control.Exception            (SomeException)
import Control.Monad.Fix            (fix)
import Control.Monad.IO.Class

retryCancel :: Async a -> Int -> IO (Either SomeException a)
retryCancel async waitingPeriod =
    fix retrying
        where retrying loop = do
                throwTo thread AsyncCancelled
                threadDelay waitingPeriod
                result <- poll async
                maybe loop return result
              thread = asyncThreadId async

retry :: MonadIO m => Int -> Int -> (a -> Bool) -> m a -> m a
retry n timeout predicate action = loop n
    where loop i = do
            result <- action
            if predicate result || i == 0
                then return result
                else liftIO (threadDelay timeout) >> loop (i-1)


